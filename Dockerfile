FROM dtzar/helm-kubectl:3.14.0

ARG HELM_VERSION=v3.14.0
ARG VALS_VERSION=0.33.1
ARG HELM_SECRETS_VERSION=4.5.1
ENV HELM_HOME /helm
ENV HELM_SECRETS_BACKEND=vals
ENV DESIRED_VERSION=${HELM_VERSION}

# Dependencies
RUN apk --update add jq curl tar openssl

# Vals
RUN curl -L https://github.com/helmfile/vals/releases/download/v${VALS_VERSION}/vals_${VALS_VERSION}_linux_amd64.tar.gz -o -| tar xz -C /usr/bin/
RUN chmod +x /usr/bin/vals

# Helm Secrets
RUN helm plugin install https://github.com/jkroepke/helm-secrets --version v${HELM_SECRETS_VERSION}
